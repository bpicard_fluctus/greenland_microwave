#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import matplotlib
import matplotlib.pyplot as plt

from cartopy import crs as ccrs
import cartopy.feature as cfeature

def default_params():
    
    matplotlib.rcParams['figure.figsize'] =  (15, 10)

    matplotlib.rcParams['font.size'] =  16

    matplotlib.rcParams['axes.titlesize'] =  30

    matplotlib.rcParams['axes.labelsize'] =  'x-large'
    matplotlib.rcParams['xtick.labelsize'] =  'x-large'
    matplotlib.rcParams['ytick.labelsize'] =  'x-large'

    matplotlib.rcParams['xtick.major.size'] =  8
    matplotlib.rcParams['xtick.minor.size'] =  5
    matplotlib.rcParams['ytick.major.size'] =  8
    matplotlib.rcParams['ytick.minor.size'] =  5

    matplotlib.rcParams['lines.linewidth'] =   5
    matplotlib.rcParams['lines.markersize'] =  0

    matplotlib.rcParams['legend.fontsize'] =  'x-large'
    matplotlib.rcParams['legend.frameon'] =  False

    matplotlib.rcParams['mathtext.default'] =  'regular'

    matplotlib.rcParams['axes.edgecolor'] =  'white'
    matplotlib.rcParams['axes.grid'] =  True
    matplotlib.rcParams['grid.color'] =  'white'
    matplotlib.rcParams['grid.linestyle'] =  '-'
    matplotlib.rcParams['grid.linewidth'] =  2
    matplotlib.rcParams['axes.facecolor'] =  'E9E9F1' 


#
#
#
def init_map(title='',resolution='110m'):
    
    # set default parameters
    default_params()
    matplotlib.rcParams['lines.markersize'] = 0
    
    # init projection
    proj = ccrs.NorthPolarStereo(central_longitude=-40)
    # ~ proj = ccrs.LambertAzimuthalEqualArea(central_longitude=-40,central_latitude=75)
    # init figure
    fig = plt.figure(figsize=(20,12))   
    ax = fig.add_subplot(1,1,1,projection=proj)
    # set extent
    extent = [300,335,59,84]
    ax.set_extent(extent, ccrs.PlateCarree())
    # ~ # ocean
    feature = cfeature.NaturalEarthFeature(category='physical', name='ocean', scale=resolution,
                                        edgecolor='none',   
                                        facecolor=cfeature.COLORS['water'],zorder=1)    
    ax.add_feature(feature)
    # land
    feature = cfeature.NaturalEarthFeature(category='physical', name='land', scale=resolution,
                                        edgecolor='none',   
                                        facecolor=cfeature.COLORS['land'],zorder=3)    
    ax.add_feature(feature)
    # coastlines
    ax.coastlines(resolution=resolution)
    # grid lines
    ax.gridlines(draw_labels=True, dms=True, x_inline=False, y_inline=False,zorder=2)
    # title
    ax.set_title(title)

    return fig,ax
    
#
#
#
def remove_axis_ticks_grid_3d(fig,ax):
    
    # honestly, I'm not sure if everythin below is needed
    # but it's kinda do the trick ...
    
    ax.grid(False)
    #
    # set background figure as the axis face color
    fig.set_facecolor(ax.get_facecolor())
    fig.set_edgecolor(ax.get_facecolor())
    # Remove gray panes and axis grid
    ax.xaxis.pane.fill = False
    ax.yaxis.pane.fill = False
    ax.zaxis.pane.fill = False
    #
    ax.w_xaxis.line.set_lw(0.)
    ax.w_yaxis.line.set_lw(0.)
    ax.w_zaxis.line.set_lw(0.)
    #
    ax.xaxis.set_ticklabels([])
    ax.yaxis.set_ticklabels([])
    ax.zaxis.set_ticklabels([])
    for line in ax.xaxis.get_ticklines():
        line.set_visible(False)
    for line in ax.yaxis.get_ticklines():
        line.set_visible(False)
    for line in ax.zaxis.get_ticklines():
        line.set_visible(False)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_zticks([])
    
    return fig,ax

#
#
#
def init_3d(title=''):
    
    # set default parameters
    default_params()
    # to remove the markers at the edge of the axis
    # ~ matplotlib.rcParams['lines.markersize'] = 0
    # init figure
    fig = plt.figure()
    # set 3d
    ax = plt.subplot(111, projection='3d')
    # remove all axes lines, ticks, gridlines ...
    remove_axis_ticks_grid_3d(fig,ax)
    # larger latitudes on top
    ax.invert_xaxis()
    # Adjust plot view
    ax.view_init(elev=40, azim=+15)
    ax.dist=8
    ax.set_box_aspect(aspect = (10,5,1))
    ax.set_xlim(+85,+60)
    ax.set_ylim(-70,-20)
    # title
    ax.set_title(title,y=0.9)

    return fig,ax
