#! /usr/bin/env python3
# -*- coding: utf-8 -*-


# References
# 
# Morlighem M. et al., (2017), 
# BedMachine v3: Complete bed topography and ocean bathymetry mapping of Greenland from multi-beam echo sounding combined with mass conservation, 
# Geophys. Res. Lett., 44, doi:10.1002/2017GL074954. (http://onlinelibrary.wiley.com/doi/10.1002/2017GL074954/full)
#
# Morlighem, M. et al. 2021, updated 2021. IceBridge BedMachine Greenland, Version 4. [Indicate subset used]. 
# Boulder, Colorado USA. NASA National Snow and Ice Data Center Distributed Active Archive Center. 
# doi: https://doi.org/10.5067/VLJ5YXKCNGXO. [Date Accessed].
#
# https://nsidc.org/data/IDBMG4/versions/4


import xarray as xr
import geopandas as gpd
import rioxarray
from shapely.geometry import mapping

import os, sys

import numpy as np

import pyproj

import matplotlib
import matplotlib.pyplot as plt
from matplotlib.pyplot import get_cmap

import lib_cartopy as lc

# ========================================================================
#
#
#
# ========================================================================

# the initial file size is about 2.Gb 
# let's subsample it to improve the computation time
# load full DEM
# ~ ds = xr.open_dataset('./BedMachineGreenland-2021-04-20.nc')
# subsample
# ~ ds = ds.isel(x=slice(None, None, 10), y=slice(None, None, 10))
# dump
# ~ ds.to_netcdf('files/BedMachineGreenland-2021-04-20_subsampled.nc','w')


# load
ds = xr.open_dataset('files/BedMachineGreenland-2021-04-20_subsampled.nc')
# add longitudes / latitudes to the dataset
# Coordinate system of the file
source_crs = 'epsg:3413' 
# Global lat-lon coordinate system
target_crs = 'epsg:4326' 
# transformation function
polar_to_latlon = pyproj.Transformer.from_crs(source_crs, target_crs)
# mesh the coordinates
X, Y = np.meshgrid(ds.x, ds.y)
# transform to longitudes / latitudes
lat, lon = polar_to_latlon.transform(X, Y)
# add coordinates to the dataset
ds.coords['latitude']  = (ds.surface.dims, lat)
ds.coords['longitude'] = (ds.surface.dims, lon)

# init figure
fig,ax = lc.init_3d(title='Greenland\nAltitude of the surface [m]')
# get cmap and defined nblevels
cmap = matplotlib.cm.get_cmap('YlGnBu_r', 7)
# Plot surface
plot = ax.plot_surface(X=ds.latitude, Y=ds.longitude, Z=ds['surface'].values, cmap=cmap,vmin=0,vmax=3500)
# Add colorbar
cbar = fig.colorbar(plot, ax=ax, shrink=0.6, pad=+0.0)
# dump
fname = 'greenland_surface_non-clipped.png'
plt.savefig(fname,bbox_inches='tight')


# load shapefile
geodf = gpd.read_file('files/sd368wz2435.shp')
# load DEM
# subsampled version
ds = xr.open_dataset('files/BedMachineGreenland-2021-04-20_subsampled.nc')
# write crs for compatibility with rio.clip
# note that the proj4 string in the netcdf file does not respect the new standard
# '+init=epsg:3413' should be 'epsg:3413'
# this raises a warning if not corrected
ds = ds.rio.write_crs(ds.proj4[6:])
# clip the xarray dataset to the shapefile geodf
print('clip...')
ds = ds.rio.clip(geodf.geometry.apply(mapping), geodf.crs)
# some weird values appeared (1e36) probably due to NaN values
# filtering out the spurious values
ds = ds.where(ds.surface < 5000)
# dump
ds.to_netcdf('files/BedMachineGreenland-2021-04-20_subsampled_clipped.nc')

# ================================
# init figure
fig,ax = lc.init_3d(title='Greenland\nAltitude of the surface [m]')
# get cmap and defined nblevels
cmap = matplotlib.cm.get_cmap('YlGnBu_r', 7)
# Plot surface
plot = ax.plot_surface(X=ds.latitude, Y=ds.longitude, Z=ds['surface'].values, cmap=cmap,vmin=0,vmax=3500)
# Add colorbar
cbar = fig.colorbar(plot, ax=ax, shrink=0.6, pad=+0.0)
# dump
fname = 'greenland_surface_clipped.png'
plt.savefig(fname,bbox_inches='tight')


