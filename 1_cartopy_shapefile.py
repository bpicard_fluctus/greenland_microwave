#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# purpose:
# load a shapefile with cartopy and display the map

# references
# Hijmans, Robert J.. University of California, Berkeley. Museum of Vertebrate Zoology. Boundary, Greenland, 2015. [Shapefile]. 
# University of California, Berkeley. Museum of Vertebrate Zoology. 
# Retrieved from https://maps.princeton.edu/catalog/stanford-sd368wz2435 

import matplotlib.pyplot as plt
from cartopy.io.shapereader import Reader as creader
from cartopy import crs as ccrs

import lib_cartopy as lc

# resolution of coastlines and features
resolution = '10m'
# init map
fig,ax = lc.init_map(title='Greenland\nadd shapefile with cartopy',resolution=resolution)
# read and add shapefile
shp = creader('files/sd368wz2435.shp')
ax.add_geometries(shp.geometries(),ccrs.PlateCarree(), facecolor='green',zorder=4)
# save
plt.savefig('greenland_shapefile.png',bbox_inches='tight')


