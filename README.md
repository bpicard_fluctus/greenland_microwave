# Greenland microwave

## Observation of Greenland by microwave radiometers

This serie of notebooks presents the results of preliminary activities to a scientific study to be conducted for CNES (French spatial agency)
with the collaboration of Météo France.<br> 
The study is focusing on the observation of Greenland using specific instruments (microwave radiometers) on-board satellite missions.<br>
The preliminary activities consist on preparation of data handling and data representation.<br>


* Read a shapefile and display with cartopy

* Convert drainage systems (boundaries) to polygon shapefile

* Apply shapefile to mask an xarray dataset (also a 3d map of the surface)


![drainages](/greenland_surface_clipped.png?raw=true)
![drainages](/greenland_drainages_systems_ekholm.png?raw=true)
