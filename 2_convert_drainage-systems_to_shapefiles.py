#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# References
# 
# Zwally, H. J., Jun LI, Anita C. Brenner, Matthew Beckley, Helen G. Cornejo, John DiMarzio, 
# Mario B. Giovinetto, Thomas A. Neumann, John Robbins, Jack L. Saba, Donghui Yi, Weili Wang, 2011, 
# “Greenland ice sheet mass balance: distribution of increased mass loss with climate warming; 200307 versus 19922002,” 
# Journal of Glaciology, Vol. 57, No. 201, pp 88-102, 2011.
#
# Zwally, H. Jay, Mario B. Giovinetto, Matthew A. Beckley, and Jack L. Saba, 2012, 
# Antarctic and Greenland Drainage Systems, GSFC Cryospheric Sciences Laboratory, 
# http://icesat4.gsfc.nasa.gov/cryo_data/ant_grn_drainage_systems.php
# https://earth.gsfc.nasa.gov/cryo/data/polar-altimetry/antarctic-and-greenland-drainage-systems


# purpose:
# convert drainage system limits into polygons for shapefile



import numpy as np
import xarray as xr

import geopandas as gpd
import shapely
from shapely.geometry import Polygon

import colorcet as cc

from cartopy.io.shapereader import Reader as creader
from cartopy import crs as ccrs

import matplotlib.pyplot as plt

import lib_cartopy as lc


#
# load txt, convert to netcdf
# ==============================
buf = np.loadtxt('files/grndrainagesystems_ekholm.txt',skiprows=7)
ds = xr.Dataset({'grndrainagesystems':(['ncells'],buf[:,0])},
                    coords={'latitude':('ncells',buf[:,1]),'longitude':('ncells',buf[:,2]),'ncells':('ncells',range(buf[:,0].size))})
ds.to_netcdf('files/grndrainagesystems_ekholm.nc','w')
ds = xr.open_dataset('files/grndrainagesystems_ekholm.nc')
lon = ds.longitude.values
lon[lon>180] = lon[lon>180]-360  
minlon,maxlon = ds.longitude.min(),ds.longitude.max()
minlat,maxlat = ds.latitude.min(), ds.latitude.max()
da = ds['grndrainagesystems']

# conversion of limits to a list of polygons
# ==============================
# unique values of the region identifier
list_of_regions = list(np.unique(da.values))
# init list of polygons for each reagions
list_of_polygons = []
# loop regions
for regval in list_of_regions:
    daf = da.where(da==regval,drop=True)
    lon_point_list = daf.longitude.values
    lat_point_list = daf.latitude.values
    list_of_polygons.append(Polygon(zip(lon_point_list , lat_point_list)))
polygons = shapely.geometry.MultiPolygon(list_of_polygons)
# dump to shapefile
# WGS 84 / NSIDC Sea Ice Polar Stereographic North
crs = 'epsg:3413'
polygon = gpd.GeoDataFrame(index=[0], crs=crs, geometry=[polygons])     
shpfilename = 'files/grndrainagesystems_ekholm.shp'  
polygon.to_file(filename=shpfilename, driver="ESRI Shapefile")


# cartopy
# ==============================
# resolution of coastlines and features
resolution = '10m'
# colormap
colors=cc.glasbey_cool
# init map
fig,ax = lc.init_map(title='Greenland\ndrainage systems',resolution=resolution)
# read and add regions
shp = creader(shpfilename)
multipolygon = list(shp.records())[0]
list_of_polygons = list(multipolygon.geometry.geoms)
for ip,poly in enumerate(list_of_polygons):    
    ax.add_geometries([poly],ccrs.PlateCarree(),facecolor=colors[ip],zorder=4)
# save
plt.savefig('greenland_drainages_systems_ekholm.png',bbox_inches='tight')
